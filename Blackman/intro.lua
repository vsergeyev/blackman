-----------------------------------------------------------------------------------------
--
-- intro.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local group = nil

local label1, label2, light = nil, nil, nil

-- local windSound = audio.loadStream("sounds/wind.mp3")
local zzzSound = audio.loadStream("sounds/zzz.wav")
local onSound = audio.loadStream("sounds/on.wav")
local wtfSound = audio.loadStream("sounds/wtf.wav")

------------------------------------------------------------------------------
local function runLevel( e )
	storyboard.gotoScene( "level", "fade", 500 )
	return true
end

------------------------------------------------------------------------------

local function zzzZzz( e )
	local function wtFuck( e )
		label2.text = "?!?!?!" -- "WTF?!"
	end
	label1.alpha =0.5
	label1.text = "touch to skip"

	label2 = display.newText( "Z z z", 50, 12, native.systemFont, 12)
	label2.x, label2.y = 60, 100
	label2.alpha = 0.2
	label2:setTextColor(200)
	group:insert( label2 )

	transition.to(label2, {time=1000, x=label2.x-20, y=label2.y-20, alpha=1})
	transition.to(label2, {time=1000, delay=1000, alpha=0})
	transition.to(label2, {time=1, delay=3000, x=60, y=100, alpha=0.2})

	transition.to(label2, {time=1000, delay=3200, x=label2.x-20, y=label2.y-20, alpha=1})
	transition.to(label2, {time=1000, delay=4200, alpha=0})
	transition.to(label2, {time=1, delay=6000, x=60, y=100, alpha=0.2})
	transition.to(label2, {time=1000, delay=6200, x=label2.x-20, y=label2.y-20, alpha=1, onComplete=wtFuck})
end

------------------------------------------------------------------------------

local function lightOn()
	audio.setVolume(0.3, {channel=3})
	audio.play(onSound, {channel=3})

	light = display.newLine(300, -50, 100, 100 + screenH)
	light.width = 150
	light.alpha = 0.3
	group:insert(light)

	transition.to(light, {time=1000, delay=1000, x=light.x-80, alpha=0.6})
end

------------------------------------------------------------------------------

local function thisIsSparta( e )
	for i = group.numChildren, 1, -1 do
		group[i]:removeSelf()
	end
	
	audio.play(wtfSound)

	local bg0 = display.newImageRect("i/sparta.png", 480, 320)
	bg0:setReferencePoint(display.TopLeftReferencePoint)
	bg0.x, bg0.y = 0, 0
	group:insert(bg0)

	label2 = display.newText( "THIS IS SPARTA!!!!!!!!", 200, 50, native.systemFont, 32)
	-- label2 = display.newText( "WTF!!!", 200, 50, native.systemFont, 32)
	label2.x, label2.y = 280, 150
	label2.alpha = 1
	label2:setTextColor(200)
	group:insert( label2 )

	transition.to(label2, {time=500, alpha=0})
	transition.to(label2, {time=500, delay=500, alpha=1})
	transition.to(label2, {time=500, delay=1000, alpha=0})
	transition.to(label2, {time=500, delay=1500, alpha=1, onComplete=runLevel})
end

------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	group = self.view

	local g = graphics.newGradient(
	  { 40, 40, 40 },
	  { 100, 100, 100 },
	  "down" )

	local bg0 = display.newImageRect("i/starmap.png", 480, 320)
	bg0:setReferencePoint(display.TopLeftReferencePoint)
	bg0.x, bg0.y = 0, -50
	group:insert(bg0)

	local bg = display.newRect( 0, -halfH, screenW, screenH + halfH - 50 )
	bg:setFillColor( g )
	bg.alpha = 0.7
	group:insert(bg)

	for i=1, 5, 1 do
		local dx = math.random(20)
		local tree = display.newImageRect( "i/tree.png", 140 + dx, 140 + dx )
		tree:setReferencePoint(display.BottomLeftReferencePoint)
		tree.x, tree.y = -40 + 140*(i-2), screenH - 47
		tree.alpha = 0.1
		group:insert(tree)
	end

	for i=1, 8, 1 do
		local dx = math.random(20)
		local tree = display.newImageRect( "i/tree.png", 80 + dx, 80 + dx )
		tree:setReferencePoint(display.BottomLeftReferencePoint)
		tree.x, tree.y = 80*(i-2) + dx, screenH - 47
		tree.alpha = 0.3
		group:insert(tree)
	end

	for i=1, 5, 1 do
		local grass = display.newImageRect( "i/grass.png", 150, 10 )
		grass:setReferencePoint(display.BottomLeftReferencePoint)
		grass.x, grass.y = 150*(i-1), screenH - 47
		group:insert(grass)
	end

	local home = display.newImageRect("i/home.png", 200, 200)
	home.x, home.y = 100, 170
	group:insert(home)

	label1 = display.newText( "Once upon a time...", 200, 12, native.systemFont, 12)
	label1:setReferencePoint( display.TopLeftReferencePoint )
	label1.x, label1.y = 160, screenH - 40
	label1:setTextColor(200)
	group:insert( label1 )

	audio.play(zzzSound, {loops=2})

	-- Intro handlers
	timer.performWithDelay(3000, zzzZzz, 1 )
	timer.performWithDelay(9000, lightOn, 1 )
	timer.performWithDelay(15000, thisIsSparta, 1 )

	-- Skip intro if player wants
	label1:addEventListener('touch', runLevel)
	bg:addEventListener('touch', runLevel)
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------


return scene