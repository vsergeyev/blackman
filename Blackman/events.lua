-----------------------------------------------------------------------------------------
--
-- events.lua
--
-----------------------------------------------------------------------------------------

-- THE EXPLOSION FUNCTION
local particles = {} -- particle table
function explosion (theX, theY, white)  -- blood is BOOL
        local particleCount = 25 -- number of particles per explosion 
        for  i = 1, particleCount do
                local theParticle = {}
                theParticle.object = display.newCircle(theX,theY,3)
                if white then
                	theParticle.object:setFillColor(255)
                else
                	theParticle.object:setFillColor(0,0,0)
                end
                theParticle.xMove = math.random (10) - 5
                theParticle.yMove = math.random (5) * - 1
                theParticle.gravity = 0.5
                table.insert(particles, theParticle)
        end
end
 
 
-- PARTICLES MOVING
function animation ()
        for i,val in pairs(particles) do
           -- move each particle
                        val.yMove = val.yMove + val.gravity
                        val.object.x = val.object.x + val.xMove
                        val.object.y = val.object.y + val.yMove
           
           -- remove particles that are out of bound                            
           if val.object.y > display.contentHeight or val.object.x > display.contentWidth or val.object.x < 0 then 
                        val.object:removeSelf();
                        particles [i] = nil
           end                            
        end
end

------------------------------------------------------------------------------

function pauseResume( e )
	if e.phase == "began" then
		if isPause then
			isPause = false
			physics.start()
		else
			isPause = true
			physics.pause()
		end
	end
	return true
end

------------------------------------------------------------------------------

function fireEnergy( e )
	if e.phase == "began" then
		-- Bullet costs Actor % of dark energy/life
		man.alpha = man.alpha - 0.01

		local bullet = display.newRect(man.x, man.y, 9, 3)
		bullet.name = "bullet"
		bullet:setFillColor(0)
		physics.addBody(bullet, {density=0.0})
		bullet.isFixedRotation = true
		bullet.collision = collisionBullet
		bullet:addEventListener( "collision",  bullet)
		group:insert( bullet )
		bullet:applyLinearImpulse(0.2, 0, bullet.x, bullet.y)
	end
	return true
end

------------------------------------------------------------------------------

function collisionMan( self, e )
    if isPause then return end

    if (e.phase == 'began') then
        if e.other.name == 'energy' and e.other.alpha > 0 then
            e.target.alpha = e.target.alpha + 0.1
            e.other.alpha = 0
            explosion (e.target.x, e.target.y)
			Runtime:addEventListener( "enterFrame", animation)
		elseif e.other.name == 'white' and e.other.alpha > 0 then
			e.target.alpha = e.target.alpha - 0.1
			e.other.alpha = 0
        end
    end
end

------------------------------------------------------------------------------

function collisionBullet( self, e )
    if isPause then return end

    if (e.phase == 'began') then
        if e.target.alpha > 0 and e.other.name == 'white' and e.other.alpha > 0 then
        	e.target.alpha = 0
            e.other.alpha = 0
            explosion (e.target.x, e.target.y, true)
			Runtime:addEventListener( "enterFrame", animation)
			hitsWhite = hitsWhite + 1
        end
    end
end

------------------------------------------------------------------------------

function soundWind()
	audio.setVolume(0.05, {channel=1})
	audio.play(windSound, {channel=1, loops=-1})
end

------------------------------------------------------------------------------

function decreaseLife( e )
	if isPause then return end

	man.alpha = man.alpha - 0.04

	if man.alpha <= 0 then
		isPause = true
		local alert = native.showAlert( "Game over", "Dark Hedgehog disappeared :(", { "OK" } )
	end
end

------------------------------------------------------------------------------

function speedUp( e )
	if isPause then return end

	if speed < 8 then
		speed = speed + 0.5
	end
end

------------------------------------------------------------------------------

function jumpMan( e )
	if isPause then
		isPause = false
		physics.start()
		man:play()
		return
	end

	if e.phase == "began" and man.y > 220 then
		man:applyLinearImpulse(0.01, -0.20, man.x, man.y)
	end
end

------------------------------------------------------------------------------

function moveBg( e )
	if isPause then return end

	runPath = runPath + speed
	updateLabels()

	for i = 1, group.numChildren, 1 do
		if group[i].name == "grass" then
			group[i].x = group[i].x - speed
		end
		if group[i].name == "tree1" then
			group[i].x = group[i].x - speed
		end
		if group[i].name == "energy" or group[i].name == "white" then
			group[i].x = group[i].x - speed
		end
		if group[i].name == "tree2" then
			group[i].x = group[i].x - 2 * speed
		end
		-- if group[i].name == "bullet" then
		-- 	group[i].x = group[i].x + 4
		-- end
    end
end

------------------------------------------------------------------------------

function frameHandler( e )
	if isPause then return end

	moveBg()

	-- Shift background if Actor jump too high
	local maxY = 50
	if man.y < maxY then
		if group.y < 100 then
			group.y = group.y + maxY - man.y
		end
		man.y = maxY
	else
		if group.y > 0 then
			group.y = group.y - 2
		end
	end
	if man.x > 50 then
		man.x = man.x - 0.2
	end
	if man.x > 300 then
		man:applyLinearImpulse(-0.1, 0, man.x, man.y)
	end

	-- Remove off-screen objects
	for i = group.numChildren, 1, -1 do
		local g = group[i]
		-- Remove off-screen bullets
		if g.name == "bullet" and g.x > screenW then
			g:removeSelf()
		end

		if g.clearOffscreen and g.x < -200 then
			g:removeSelf()
		end
	end
end
