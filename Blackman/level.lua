-----------------------------------------------------------------------------------------
--
-- level.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local rain = require("rain")

group, groupControls = nil, nil
bg, bg2 = nil, nil
man = nil
speed = 1
runPath = 0
hitsWhite = 0
label1 = nil

windSound = audio.loadStream("sounds/wind.mp3")

physics = require "physics"
physics.start()
physics.pause()

-- physics.setDrawMode('hybrid')
physics.setGravity(0, 10)

require "events"
require "actors"

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--       unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

function updateLabels()
	label1.text = "Dark energy: " .. math.round(man.alpha*100) .. "%  / " .. math.round(runPath) .. "m / " .. "Light sources destroyed: " .. hitsWhite
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
    group = display.newGroup() -- self.view
    groupControls = display.newGroup()
    addBg()
    addTree()
    addMan()
	addEnergy()
	addGrass()

	addControls()
	
	timer.performWithDelay(1, soundWind, 1 )
	timer.performWithDelay(2000, decreaseLife, 0 )
	timer.performWithDelay(10000, speedUp, 0 )

	Runtime:addEventListener( "enterFrame", frameHandler )

	rain.new(group, {})
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
    local group = self.view
	physics.start()
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
    local group = self.view
    physics.pause()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
    local group = self.view

end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene