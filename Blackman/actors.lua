-----------------------------------------------------------------------------------------
--
-- actors.lua
--
-----------------------------------------------------------------------------------------

local movieclip = require "movieclip"

require "events"

-----------------------------------------------------------------------------------------

function addMan()
	local home = display.newImageRect("i/home.png", 200, 200)
	home.x, home.y = 0, 170
	home.name = "tree2"
	home.clearOffscreen = true
	group:insert(home)

	--man = display.newImageRect( "i/blackman2.png", 60, 60 )
	man = movieclip.newAnim({"i/blackman2.png", "i/blackman2-2.png", "i/blackman2-3.png"}, 60, 60)
	man:setSpeed(.1)
	man.x, man.y = 100, screenH - 80
	physics.addBody( man, {bounce=0.5, radius=25, friction=0.2} )
	man.isFixedRotation = true
	man.collision = collisionMan
	group:insert(man)
	man:addEventListener( "collision",  man)
end

-----------------------------------------------------------------------------------------

function addBg()
	local g = graphics.newGradient(
	  { 40, 40, 40 },
	  { 100, 100, 100 },
	  "down" )

	local bg0 = display.newImageRect("i/starmap.png", 480, 320)
	bg0:setReferencePoint(display.TopLeftReferencePoint)
	bg0.x, bg0.y = 0, -100

	bg = display.newRect( 0, -halfH, screenW, screenH + halfH - 50 )
	bg:setFillColor( g )
	bg.alpha = 0.7

	bg2 = display.newRect( 0, screenH - 50, screenW, 50 )
	bg2:setFillColor( 0 )
	physics.addBody( bg2, 'static', {bounce=0.2, friction=0.5} )

	group:insert(bg0)
	group:insert(bg)
	group:insert(bg2)
	bg:addEventListener('touch', jumpMan)
	bg2:addEventListener('touch', jumpMan)
end

-----------------------------------------------------------------------------------------

function addTree()
	for i=1, 50, 1 do
		local dx = math.random(20)
		local tree = display.newImageRect( "i/tree.png", 140 + dx, 140 + dx )
		tree:setReferencePoint(display.BottomLeftReferencePoint)
		tree.x, tree.y = 150*(i-2), screenH - 47
		tree.alpha = 0.1
		tree.name = "tree1"
		tree.clearOffscreen = true
		group:insert(tree)
	end

	for i=1, 300, 1 do
		local dx = math.random(20)
		local tree = display.newImageRect( "i/tree.png", 80 + dx, 80 + dx )
		tree:setReferencePoint(display.BottomLeftReferencePoint)
		tree.x, tree.y = 80*(i-2) + dx, screenH - 47
		tree.alpha = 0.3
		tree.name = "tree2"
		tree.clearOffscreen = true
		group:insert(tree)
	end
end

-----------------------------------------------------------------------------------------

function addEnergy()
	for i=1, 50, 1 do
		local energy = nil
		local dx = 300 + math.random(100)
		local dy = math.random(100)

		if math.random(3) > 1 then
			energy = movieclip.newAnim({"i/energy.png", "i/energy2.png"})
			energy.name = "energy"
			energy.x, energy.y = dx + i*200, 80 + dy
		else
			energy = movieclip.newAnim({"i/white.png", "i/white2.png"})
			energy.name = "white"
			energy.x, energy.y = dx + i*200, screenH - 80
		end

		physics.addBody(energy, "static", {radius=25})
		energy.isSensor = true
		energy.clearOffscreen = true
		group:insert(energy)
		energy:setSpeed(.1)
		energy:play()
	end
end

-----------------------------------------------------------------------------------------

function addGrass()
	for i=1, 150, 1 do
		local grass = display.newImageRect( "i/grass.png", 150, 10 )
		grass:setReferencePoint(display.BottomLeftReferencePoint)
		grass.x, grass.y = 150*(i-1), screenH - 47
		grass.name = "grass"
		grass.clearOffscreen = true
		group:insert(grass)
	end
end

-----------------------------------------------------------------------------------------

function addControls()
	label1 = display.newText( "", 200, 12, native.systemFont, 12)
	label1:setReferencePoint( display.TopLeftReferencePoint )
	label1.x, label1.y = 220, screenH - 40
	label1:setTextColor(200)
	groupControls:insert( label1 )

	local pauseBtn = display.newRect(screenW - 30, 0, 30, 30)
	-- pauseBtn:setFillColor(0)
	pauseBtn.alpha = 0.2
	pauseBtn.strokeWidth = 2
	pauseBtn:setStrokeColor(100)
	pauseBtn:addEventListener('touch', pauseResume)
	groupControls:insert( pauseBtn )

	local btnText = display.newText( "||", 50, 30, native.systemFont, 16)
	btnText.x, btnText.y = screenW - 15, 15
	btnText:setTextColor(0)
	groupControls:insert( btnText )

	local fireBtn =  display.newImageRect("i/white.png", 30, 30) --display.newCircle(30, screenH-25, 16)
	fireBtn.x, fireBtn.y = 30, screenH-25
	-- fireBtn:setFillColor(200, 0, 0)
	fireBtn.strokeWidth = 2
	fireBtn:setStrokeColor(150, 0, 0)
	fireBtn:addEventListener('touch', fireEnergy)
	groupControls:insert( fireBtn )
end

-----------------------------------------------------------------------------------------
